//Global Values that does npt change
var eleClasses = ["directionIcon", "directionLable", "directionFirstLetter"];
var directionArray = ["Up", "Down", "Right", "Left"];
var colors = ["#EC407A", "#8E24AA", "#C2185B", "#673AB7", "#3F51B5", "#2196F3", "#3D5AFE", "#009688", "#43A047", "#E65100", "#E64A19"];
wrightOrWrongIterate = 10;
solidDirectionRandNum = 356;

//Global Values
var prevDirection, currentDirection;

//defaults values for app start and restart
function defaults() {
    creationTimer = 0, positionTimer = 0, blinkTimer = 0, audioElement = 0, heartLevel = 0, blinkRandomValue = 485;
    totalScore = 0;
    elementsList = [];
    positionTimerInterval = 1000;
    elementCreateInterval = 3000;
    gamelevel = 1;
    currentlevel = 1;
    initialTopPosition = 60;
    increasingTopPosition = 10;
    positionArray = [];
    enableSound = localStorage.getItem("sidEffectsEnableSound") ? JSON.parse(localStorage.getItem("sidEffectsEnableSound")) : true;
    highScore = 0;
    specialGameMode = 0;
    progressBarWidth = 0;
    powerButtonArray = [];
    isInverseMode = false;
    isBlinkMode = false;
    isRightOrWrongMode = false;
    isPrevSpecialLevel = false;
    isInvisibleMode = false;
}

//For Cordova
var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },
    onDeviceReady: function () {
        this.receivedEvent('deviceready');
    },
    receivedEvent: function (id) {
        navigator.splashscreen.hide();
        $('.exit').off("click").on('click', function () {
            exitFromApp();
        });
        if (window.MobileAccessibility) {
            window.MobileAccessibility.usePreferredTextZoom(false);
        }
    }
};

app.initialize();
defaults();
//Document Ready     
$(document).ready(function () {
    showDirectionIcons();
    if ($(".introContainer").is(":visible")) {
        $("#dangerZone").hide();
        $("#homebtn").hide();
        $("#levelWrapper").hide();
        $("#gameOverWrapper").hide();
        //  setPlayAnimation();
    }
    //Play Button click
    $("#playWrapper").off("click").on("click", function () {
        if ($(".introContainer").is(":visible")) {
            $(".introContainer").hide();
            $("#playWrapper").hide();
            $("#game-overlay").show();
            $('#myModal').show();
            window.getStart = setInterval(countDown, 1000);
        } else {
            $("#playWrapper").removeClass("pause").hide();
            $("#playWrapper .play").addClass("rotate");
            $("#homebtn").hide();
            $("#game-overlay").hide();
            setPositionInterval();
            newElementCreateInterval();
        }
        if ($("#playWrapper").hasClass("pause")) {
            pause();
        }
        window.clearInterval(window.animatePlayBtn);
    });
    if (!enableSound) {
        $("#setMusic").removeClass("volume-on").addClass("volume-off");
    }
    doLogoAnimation();
    renderHighScore();
    registerEvents();
});
//Direction Icon Creation
function showDirectionIcons() {
    if ($(".directionButtonCont").children()) {
        $(".directionButtonCont").children().remove();
    }
    directionElement = [];
    directionElement.push('<div class="arrow left directionBtn" value="Left"></div>');
    directionElement.push('<div class="center directionBtn">');
    directionElement.push('<div class="arrow up directionBtn" value="Up"></div>');
    directionElement.push('<div class="arrow down directionBtn" value="Down"></div>');
    directionElement.push('</div>');
    directionElement.push('<div class="arrow right directionBtn" value="Right"></div>');

    $(".directionButtonCont").append(directionElement.join(''));
}

//Logo Animation
function doLogoAnimation() {
    window.setLogoAnimation = setInterval(function () {
        $(".orgName.jump").addClass("bounce animated");
    }, 2000);
}
//Setup HighScore values
function renderHighScore() {
    var score = (localStorage.getItem("sidEffectsHighScore") ? localStorage.getItem("sidEffectsHighScore") :
        0);
    $(".highScoreLbl").html(score);
}
//Initial countdown timer
var countDown = function () {
    progressBarWidth = $(".bodyContainer").width() / 25;
    var count = parseInt($("#myModal").html());
    var curColor = colors[Math.floor(Math.random() * colors.length)];
    if (count !== 0 && $("#myModal").html() != "Go") {
        if (count - 1 == 0)
            $("#myModal").html("Go");
        else
            $("#myModal").html(count - 1);
    } else {
        $('#myModal').hide();
        $("#myModal").html("3");
        $("#game-overlay").hide();
        for (var i = 0; i <= 4; i++) {
            positionArray.push((($(".bodyContainer").width() / 5) * i));

        }
        $("#levelWrapper").css("color", curColor).show();
        $("#levelWrapper").addClass("slideInandOut animated");

        //createAudioElement("dissolve");
        setPositionInterval();
        newElementCreateInterval();
        registerEvents();
        stopCountDown();
    }
}
//Stop Count Down timer
var stopCountDown = function () {
    clearInterval(window.getStart);
}

//check for the game is starting for the first time or not
if (!localStorage.getItem("sideEffectsNewUser")) {
    localStorage.setItem("sideEffectsNewUser", true);
}

//Set High Score
function setHighScore() {
    var previousHighScore = localStorage.getItem("sidEffectsHighScore")
    if (previousHighScore) {
        if (previousHighScore < totalScore) {
            localStorage.setItem("sidEffectsHighScore", totalScore);
        }
    } else {
        localStorage.setItem("sidEffectsHighScore", totalScore);
    }
}
// random interval for creating new direction element
function newElementCreateInterval() {
    var rand = Math.round(Math.random() * (elementCreateInterval)) + 500;
    creationTimer = setTimeout(function () {
        getElement(creationTimer);
        newElementCreateInterval();
    }, rand);
}
//Reducing the time delay for creating the new direction element
function increasegetElementInterval() {
    if (currentlevel != gamelevel && elementCreateInterval >= 700) {
        elementCreateInterval -= 250;
        clearInterval(creationTimer);
        currentlevel = gamelevel;
    }
}
//Setup the time delay for moving down the elements in the body container
function setPositionInterval() {
    positionTimer = setInterval(function () {
        increasePosition();
    }, positionTimerInterval);
}
//Moving down the elements in the body container
function increasePosition() {
    var directionLableHeight = $(".directionLblCont").height();
    var bodyContainerHeight = $(".bodyContainer").height();
    var bodyContainerTop = $(".bodyContainer").offset().top;
    var isLimitReached = false;
    if ($(".directionLblCont").length) {
        if ($(".directionLblCont").last().offset().top >= bodyContainerHeight + bodyContainerTop - directionLableHeight - 10) {
            isLimitReached = true;
        }
        if ($(".directionLblCont").last().offset().top >= bodyContainerHeight - bodyContainerTop - directionLableHeight) {
            $("#dangerZone").show();
        } else {
            $("#dangerZone").hide();
        }
    }
    if (isLimitReached) {
        endGame();
    } else {
        $(".directionLblCont").each(function (index) {
            var currentTopPosition = parseInt($(this).css("top")) + 10 + "px";
            $(this).css("top", currentTopPosition);
        });
    }
}
//Clear Overall Timer intervals
function clearTimer(timerAttr) {
    if (timerAttr) {
        clearInterval(timerAttr)
    } else {
        clearInterval(positionTimer);
        clearInterval(creationTimer);
    }
}
//Event Handling
function registerEvents() {
    //icon click event
    $(".right, .left, .up, .down").off("click").on("click", function () {
        $("#levelWrapper").finish();
        calculatePoints(this);
    });
    //Continue button click event
    $("#continueBtn").off("click").on("click", function () {
        $("#continueBtn").hide();
        $("#game-overlay").hide();
        setPositionInterval();
        increasegetElementInterval();
        newElementCreateInterval();
    });
    //Enable or disable the sound
    $("#setMusic").off("click").on("click", function () {
        if ($(this).hasClass("volume-on")) {
            $(this).removeClass("volume-on").addClass("volume-off")
            enableSound = false;
        } else {
            $(this).removeClass("volume-off").addClass("volume-on")
            enableSound = true;
        }
        localStorage.setItem("sidEffectsEnableSound", enableSound);
    });
    //level container animation end event
    $("#levelWrapper").off("animationend").on("animationend", function () {
        $(this).removeClass("slideInandOut animated").hide();
    });
    //Ceyone logo animation
    $(".orgName.jump").off("animationend").on("animationend", function () {
        $(this).removeClass("bounce animated");
        clearInterval(window.setLogoAnimation);
        $(".ceyoneLogoContainer").hide();
        if (JSON.parse(localStorage.getItem("sideEffectsNewUser"))) {
            localStorage.setItem("sideEffectsNewUser", false);
            $(".descContainer").show();
            $("#playWrapper").hide();
        } else {
            $(".introContainer").show();
            $("#playWrapper").show();
        }
    });
    //Description next button click event
    $(".descNextBtn").off("click").on("click", function () {
        if ($(this).text() != "Done") {
            $(".lifeDescCont").addClass("bounceOutLeft animated").hide();
            $(this).text("Done");
        } else {
            $(".descSkip").trigger("click");
        }
    });

    $(".descSkip").off("click").on("click", function () {
        $(".descContainer").hide();
        $("#playWrapper").show();
    });
}
//Game Over function
function endGame() {
    clearTimer();
    setHighScore();
    $("#HighScore").html(localStorage.getItem("sidEffectsHighScore"));
    $(".highScoreLbl").html(localStorage.getItem("sidEffectsHighScore"));
    $("#score").html($(".scoreCard").html());
    $("#dangerZone").hide();
    $("#game-overlay").show();
    $("#gameOverWrapper").show();
}
//Calculating points for checking levels up
function calculatePoints(element) {
    var btnValue = $(element).attr("value");
    if (!prevDirection) {
        prevDirection = btnValue;
    }
    if (elementsList.indexOf(btnValue) != -1) {
        currentDirection = btnValue;
        createAudioElement("blast");
        playSound();
        $(".scoreCard").html(++totalScore);
        levelUp(totalScore);
        var removableElement = $(".directionLblCont[data-direction=" + btnValue + "]").last();
        if ($(removableElement).hasClass("solidDirection")) {
            $(removableElement).removeClass("solidDirection");
        } else {
            $(removableElement).remove();

            var sideItemIndex = elementsList.indexOf(btnValue);
            elementsList.splice(sideItemIndex, 1);
        }

        if (elementsList.length == 0)
            getElement();
        if (currentlevel != gamelevel && positionTimerInterval >= 201) {
            increaseTimer();
            setPositionInterval();
        }
        if (currentlevel != gamelevel) {
            increasegetElementInterval();
            newElementCreateInterval();
            currentlevel = gamelevel;
        }
    } else {
        $(".life").not(".uheart").last().addClass("uheart hinge animated");
        createAudioElement("error");
        playSound();
        heartLevel = gamelevel;
        if (isInvisibleMode)
            setBlockTimer(element);
        if (isRightOrWrongMode) {
            for (var i = 0; i < wrightOrWrongIterate; i++) {
                getElement();
            }
        }
        if ($(".life").not(".uheart").length == 0)
            endGame();
    }
    showProgress();
}

//Progress Bar for score container
function showProgress() {
    if (totalScore) {
        var newWidth = (totalScore % 25 ? (progressBarWidth * (totalScore % 25)) : $(".bodyContainer").width());
        $(".levelProgressBar").css("width", newWidth);
    }
    if (totalScore != 0 && totalScore % 25 == 0) {
        $(".levelProgressBar").css("width", "100%");
        $(".levelProgressBar").css("width", "0");
    }
}

//Level Up calculation
function levelUp(totalScore) {
    if (totalScore % 25 == 0) {
        gamelevel = parseInt(totalScore / 25) + 1;
        if (gamelevel == heartLevel + 2 && $(".life.uheart").length) {
            $(".life.uheart").first().removeClass("uheart");
            heartLevel = gamelevel - 1;
        }

        showCurrentLevelStatus(gamelevel);
        switchToNormalMode();
        if (gamelevel >= 6 && specialGameMode == 0 && !isPrevSpecialLevel) {
            startSpecialGameMode();
        } else {
            isPrevSpecialLevel = false;
        }
    }
}

function switchToNormalMode() {
    if (specialGameMode) {
        if (isBlinkMode) {
            clearInterval(blinkTimer);
            isBlinkMode = false;
        }
        if (isInverseMode) {
            exitInverseLevel();
            isInverseMode = false;
        }
        isRightOrWrongMode = false;
        isInvisibleMode = false;
        specialGameMode = 0;
    }
}

//Show current level on level up
function showCurrentLevelStatus(status) {
    var curStatus = (isNaN(status) ? status : "Level " + status)
    $(".level").html(curStatus);
    $("#levelWrapper").css("color", colors[Math.floor(Math.random() * colors.length)]).show();
    $("#levelWrapper").addClass("slideInandOut animated");
    $(".levelProgressBar").css("width", "0");
}
//Blick level
function startSpecialGameMode() {
    var specialLevel = Math.floor(Math.random() * blinkRandomValue);
    var curLevel = "";
    if (specialLevel % 4 == 0) {
        specialGameMode = 1;
        blinkTimer = setInterval(function () {
            $(".bodyContainer").fadeOut(500);
            $(".bodyContainer").fadeIn(200);
        }, 700);
        isPrevSpecialLevel = true;
        isBlinkMode = true;
        curLevel += "Blink <br>"
    }
    if (specialLevel % 3 == 0) {
        specialGameMode = 2;
        setInverseLevel();
        isPrevSpecialLevel = true;
        isInverseMode = true;
        curLevel += "Inverse <br>"
    }
    if (specialLevel % 6 == 0) {
        specialGameMode = 3;
        isRightOrWrongMode = true;
        isPrevSpecialLevel = true;
        curLevel += "Right/Wrong <br>"
    }
    if (specialLevel % 12 == 0) {
        specialGameMode = 4;
        isPrevSpecialLevel = true;
        isInvisibleMode = true;
        curLevel += "Invisible <br/>";
    }
    if (isInverseMode && isInvisibleMode && isBlinkMode && isRightOrWrongMode) {
        curLevel = "Boss Mode"
    }
    curLevel = (curLevel == "") ? gamelevel : curLevel;
    showCurrentLevelStatus(curLevel);
}
//exit inverse level
function exitInverseLevel() {
    $(".directionButtonCont, .headerContainer").css("background", "linear-gradient(to right, #184ec8, #155dc8, #256bc5, #3b77c1, #5283bb)");
    $(".directionButtonCont .directionBtn").removeClass("inverseLevel");
    showDirectionIcons();
    registerEvents();
}
//Setup inverse level
function setInverseLevel() {
    $(".directionButtonCont, .headerContainer").css("background", "#f44336");
    $(".directionButtonCont .directionBtn").addClass("inverseLevel");
    $(".directionButtonCont .directionBtn").each(function () {
        switch ($(this).attr("value")) {
            case "Up":
                $(this).attr("value", "Down");
                break;

            case "Down":
                $(this).attr("value", "Up");
                break;

            case "Right":
                $(this).attr("value", "Left");
                break;

            case "Left":
                $(this).attr("value", "Right");
                break;
        }
    });
}
//append elemets to the dom that are created
function getElement() {
    var direction = directionArray[Math.floor(Math.random() * directionArray.length)];
    var randomClass = eleClasses[Math.floor(Math.random() * (gamelevel <= 3 ? gamelevel : 3))];
    createElement(direction, randomClass);
}
//Create new elements
function createElement(direction, currentClass) {
    var element = [],
        newElement;
    element.push('<div class="directionLblCont" data-direction=' + direction + '>');
    switch (currentClass) {
        case "directionIcon":
            element.push('<div class="directionIcon arrow ' + direction + ' directionBtn" value=' + direction + '></div>');
            break;

        case "directionLable":
            element.push('<span class="directionLable">' + direction + '</span>');
            break;

        case "directionFirstLetter":
            element.push('<span class="directionFirstLetter">' + direction.charAt(0) + '</span>');
            break;
    }
    element.push('</div>');

    newElement = $(element.join(''));
    var solidNumber = Math.floor(Math.random() * solidDirectionRandNum);
    if (gamelevel > 5 && solidNumber % 3 == 0) {
        $(newElement).addClass("solidDirection");
    }
    formatElement(newElement)
    $('.bodyContainer').prepend(newElement);
    elementsList.push(direction);
}
//Positioning the newly created elements
function formatElement(element) {
    var leftPosition = positionArray[Math.floor(Math.random() * positionArray.length)];
    var color = colors[Math.floor(Math.random() * colors.length)];
    $(element).css({
        "left": leftPosition + 10 + "px",
        "background-color": color
    }).show();
}
//Create audio elements
function createAudioElement(audioSrc) {
    audioElement = document.createElement('audio');
    audioElement.setAttribute('src', "sounds/" + audioSrc + ".mp3");
}
//Enable Sound on icon click
function playSound() {
    if (enableSound) {
        audioElement.currentTime = 0;
        audioElement.play();
    }
}
//Reducing the time delay for moving down the elements
function increaseTimer() {
    positionTimerInterval -= 100;
    clearTimer();
}
//Restart application from the start
function reStart(isHome) {
    defaults();
    clearTimer();
    setHighScore();
    exitInverseLevel();
    $(".levelProgressBar").css("width", "0");
    $(".directionLblCont").remove();
    $(".level").html("Level 1");
    $(".scoreCard").html("0")
    $("#game-overlay").show();
    $(".life.uheart").removeClass("uheart hinge animated");
    $("#gameOverWrapper").hide();
    if (isHome) {
        $("#game-overlay").hide();
        $("#playWrapper").show().removeClass("pause");
        $("#playWrapper .play").addClass("rotate");
        $("#homebtn").hide();
        $(".introContainer").show();
        // setPlayAnimation();
    } else {
        $("#myModal").show();
        window.getStart = setInterval(countDown, 1000);
    }
}
//home button click
function home() {
    reStart(true);
}
//pause button click
function pause() {
    clearTimer();
    $("#game-overlay").show();
    $("#playWrapper").addClass("pause").show();
    $("#playWrapper .play").removeClass("rotate");
    $("#homebtn").show();
}
//exit from the app
function exitFromApp() {
    navigator.app.exitApp();
}

function share() {
    //  navigator.share("Try this awesome fun filled new game", "Share Side Effects", "plain/text");
    var imageLink, messageBody;
    console.log('Calling from CapturePhoto');
    navigator.screenshot.save(function (error, res) {
        if (error) {
            console.error(error);
        } else {
            console.log('ok', res.filePath); //should be path/to/myScreenshot.jpg
            //For android
            imageLink = res.filePath;
            messageBody = "Beat my score... Download SideEffects from the link... https://play.google.com/store/apps/details?id=org.ceyone.sideEffects  "
            window.plugins.socialsharing.share(messageBody, messageBody, 'file://' + imageLink, null);

            //For iOS
            //window.plugins.socialsharing.share(null,   null,imageLink, null)
        }
    }, 'jpg', 50, 'myScreenShot');
}

function setBlockTimer(element) {
    var start = 5;
    var timer = setInterval(function () {
        start--;
        var countdown = "0:0" + start;
        $(element).addClass("timer").html(countdown);
        if (start < 0) {
            $(element).removeClass("timer").html("");
            window.clearInterval(timer);
        }
    }, 1000);
}