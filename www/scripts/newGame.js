var directionArray = ["Up", "Down", "Right", "Left"];
var colors = ["#EC407A", "#8E24AA", "#C2185B", "#673AB7", "#3F51B5", "#2196F3", "#3D5AFE", "#009688", "#43A047", "#E65100", "#E64A19"];
var creationTimer, timer, blinkTimer, blinkRandomValue = 485;
var totalScore = 0;
var sideItems = [];
var timerSeconds = 1000;
var elementCreateInterval = 3000;
var gamelevel = 1;
var currentlevel = 1;
var initialTopPosition = 60;
var increasingTopPosition = 10;
var audioElement;
var positionArray = [];
var enableSound = localStorage.getItem("sidEffectsEnableSound")? JSON.parse(localStorage.getItem("sidEffectsEnableSound")):true;
var enableVibration = true;
var highScore = 0;
var specialGameMode = 0;
var heartLevel;
var isPrevSpecialLevel = false;
var eleClasses = ["directionIcon", "directionLable", "directionFirstLetter"];
var powerButtonArray = [];
if(!localStorage.getItem("sideEffectsNewUser"))
{
    localStorage.setItem("sideEffectsNewUser", true);
}

var powerBtn = {
    "Left" : 0,
    "Down" : 0,
    "Up"  : 0,
    "Right" : 0
};
var prevDirection, currentDirection;

function setHighScore() {
    var previousHighScore = localStorage.getItem("sidEffectsHighScore")
    if (previousHighScore) {
        if (previousHighScore < totalScore) {
            localStorage.setItem("sidEffectsHighScore", totalScore);
        }
    } else {
        localStorage.setItem("sidEffectsHighScore", totalScore);
    }
}

function newElementCreateInterval() {
    var rand = Math.round(Math.random() * (elementCreateInterval)) + 500;
    creationTimer = setTimeout(function () {
        getElement(creationTimer);
        newElementCreateInterval();
    }, rand);
}

function increasegetElementInterval() {
    if (currentlevel != gamelevel && elementCreateInterval>=0) {
        elementCreateInterval -= 400;
        clearInterval(creationTimer);
        currentlevel = gamelevel;
    }
}

function setRandomDirection() {
    timer = setInterval(function () {
        increasePosition();
    }, timerSeconds);
}

function increasePosition() {
    var directionLableHeight = $(".directionLblCont").height();
    var bodyContainerHeight = $(".bodyContainer").height();
    var bodyContainerTop = $(".bodyContainer").offset().top;
    var isLimitReached = false;
    if ($(".directionLblCont").length) {
        if ($(".directionLblCont").last().offset().top >= bodyContainerHeight + bodyContainerTop - directionLableHeight - 10) {
            isLimitReached = true;
        }
        if ($(".directionLblCont").last().offset().top >= bodyContainerHeight - bodyContainerTop - directionLableHeight) {
            $("#dangerZone").show();
        } else {
            $("#dangerZone").hide();
        }
    }
    if (isLimitReached) {
        endGame();
    } else {
        $(".directionLblCont").each(function (index) {
            var currentTopPosition = parseInt($(this).css("top")) + 10 + "px";
            $(this).css("top", currentTopPosition);
        });
    }
}

function clearTimer(timerAttr) {
    if (timerAttr) {
        clearTimeout(creationTimer);
    } else {
        clearInterval(timer);
        clearTimeout(creationTimer);
    }

}

function registerEvents() {
    $(".right, .left, .up, .down").off("click").on("click", function () {
        calculatePoints(this);
    });
    $(".pause").off("click").on("click", function () {
        pause();
    });
    $("#continueBtn").off("click").on("click", function () {
        $("#continueBtn").hide();
        $("#game-overlay").hide();
        setRandomDirection();
        increasegetElementInterval();
        newElementCreateInterval();
    });

    $("#setMusic").off("click").on("click", function () {
        if ($(this).hasClass("volume-on")) {
            $(this).removeClass("volume-on").addClass("volume-off")
            enableSound = false;
        } else {
            $(this).removeClass("volume-off").addClass("volume-on")
            enableSound = true;
        }
        localStorage.setItem("sidEffectsEnableSound", enableSound);
    });

    $("#play").off("animationend").on("animationend",function () {
        $(this).removeClass("bounce animated");
    }); 

    $("#levelWrapper").off("animationend").on("animationend", function () {
        $(this).removeClass("slideInUp animated").hide();
    });

    $(".orgName").off("animationend").on("animationend", function () {
        $(this).removeClass("jello animated");
        clearInterval(window.setLogoAnimation);
        $(".ceyoneLogoContainer").hide();
        if(JSON.parse(localStorage.getItem("sideEffectsNewUser")))
        {
            localStorage.setItem("sideEffectsNewUser", false);
            $(".descContainer").show();
            $("#playWrapper").hide();
        }
        else
        {
            $(".introContainer").show();
            $("#playWrapper").show();
        }
    });

    $(".powerBtn").off("click").on("click", function(){
        if(!$(this).hasClass("disabled"))
        {
            $(this).addClass("disabled").removeClass("bounceIn animated");
            var score = $(".directionLblCont[data-direction=" + $(this).data("direction") + "]").length;
            totalScore += score;
            $(".scoreCard").html(totalScore);
            $($(".directionLblCont[data-direction=" + $(this).data("direction") + "]")).remove();
            levelUp(totalScore);
        }
    });

    $(".descNextBtn").off("click").on("click", function(){
        if($(this).text() != "Done")
        {
            $(".lifeDescCont").addClass("bounceOutLeft animated").hide();
            $(".powerBtnDescCont").show().addClass("bounceInRight animated");
            $(this).text("Done")
        }
        else
        {
            $(".descSkip").trigger("click");
        }
    });
    
    $(".descSkip").off("click").on("click", function(){
        $(".descContainer").hide();
    });
}

function endGame() {
    clearTimer();
    clearInterval(blinkTimer);
    setHighScore();
    $("#HighScore").html(localStorage.getItem("sidEffectsHighScore"));
    $(".highScoreLbl").html(localStorage.getItem("sidEffectsHighScore"));
    $("#score").html($(".scoreCard").html());
	$("#dangerZone").hide();
    $("#game-overlay").show();
    $("#gameOverWrapper").show();
}

function calculatePoints(element) {
    var btnValue = $(element).attr("value");
    if(!prevDirection)
    {
        prevDirection = btnValue;
    }
    if (sideItems.indexOf(btnValue) != -1) {
		currentDirection = btnValue;
        createAudioElement("blast");
        playSound();
        enablePowerBtn(btnValue);
        $(".scoreCard").html(++totalScore);
        levelUp(totalScore);
        //$(".directionLblCont[data-direction=" + btnValue + "]").last().remove();
        removeDirection(btnValue);
        var sideItemIndex = sideItems.indexOf(btnValue);
        sideItems.splice(sideItemIndex, 1);
        if (sideItems.length == 0)
         getElement();
        if (currentlevel != gamelevel  && timerSeconds >= 201) {
            increaseTimer();
            setRandomDirection();  
        }
        if(currentlevel != gamelevel){
            increasegetElementInterval();
            newElementCreateInterval();
            currentlevel = gamelevel;
            console.log("timerSeconds:"+ timerSeconds)
            console.log("ElementInterval:"+ elementCreateInterval)
        }
    }
    else{
        $(".life").not(".uheart").last().addClass("uheart");
        createAudioElement("error");
        playSound();
        heartLevel = gamelevel;
        if($(".life").not(".uheart").length == 0)
            endGame();
    }
}

function enablePowerBtn(btnValue) {
    if(gamelevel >= 5)
    {
        if(prevDirection == btnValue)
        {
            powerBtn[btnValue] +=1;
            if(powerBtn[btnValue] >=4)
            {
                $(".powerBtn" + btnValue).removeClass("disabled").addClass("bounceIn animated");
            }
        }
        else
        {
            powerBtn[prevDirection] = 0;
            powerBtn[btnValue] +=1;
            prevDirection = btnValue;
        }
    }
}

function levelUp(totalScore) {
    if (totalScore % 25 == 0) {
        gamelevel = parseInt(totalScore / 25) + 1;
        if(gamelevel == heartLevel + 2 && $(".life.uheart").length){
            $(".life.uheart").first().removeClass("uheart");
            heartLevel = gamelevel -1;
        }

        showCurrentLevelStatus(gamelevel);

        if(specialGameMode == 1)
        {
            clearInterval(blinkTimer);
            specialGameMode = 0;
        }
        if(specialGameMode == 2)
        {
            exitInverseLevel();
            specialGameMode = 0;
        }
        if(gamelevel >= 7 && specialGameMode == 0 && !isPrevSpecialLevel)
        {
            startBlink();
        }
        else
        {
            isPrevSpecialLevel = false;
        }
    }
    if(gamelevel == 5)
    {
        $("#powerBtnContainer").show().addClass("bounce animated");
    }
}

function showCurrentLevelStatus(status)
{
    var curStatus = (isNaN(status) ? status : "Level " + status)
    $("#levelWrapper").css("color", colors[Math.floor(Math.random() * colors.length)]).show();
    $("#levelWrapper").addClass("slideInUp animated");
}

function startBlink()
{
    var specialLevel = Math.floor(Math.random() * blinkRandomValue);
    if(specialLevel % 4 == 0)
    {
        specialGameMode = 1;
        console.log("startBlink " + specialLevel);
        blinkTimer = setInterval(function(){
            $(".bodyContainer").fadeOut(600);
            $(".bodyContainer").fadeIn(100);
        }, 700);

        isPrevSpecialLevel = true;
    }
    if(specialLevel % 3 == 0)
    {
        specialGameMode = 2;
        setInverseLevel();
        isPrevSpecialLevel = true;
    }
}

function exitInverseLevel()
{
    $(".directionButtonCont .directionBtn, .headerContainer").css("background-color", "#006064");
    $(".directionButtonCont .directionBtn").removeClass("inverseLevel");
    showDirectionIcons();
    registerEvents();
}
function setInverseLevel()
{
    $(".directionButtonCont .directionBtn, .headerContainer").css("background-color", "#f44336");
    showCurrentLevelStatus("Inverse Mode");
    $(".directionButtonCont .directionBtn").addClass("inverseLevel");
    $(".directionButtonCont .directionBtn").each(function(){
        switch($(this).attr("value"))
        {
            case "Up":
                $(this).attr("value", "Down");
            break;

            case "Down":
                $(this).attr("value", "Up");
            break;

            case "Right":
                $(this).attr("value", "Left");
            break;

            case "Left":
                $(this).attr("value", "Right");
            break;
        }
    });
}

function getElement() {
    var direction = directionArray[Math.floor(Math.random() * directionArray.length)];
    var randomClass = eleClasses[Math.floor(Math.random() * (gamelevel <= 3 ? gamelevel : 3))];
    var newElement = createElement(direction, randomClass);
    formatElement(newElement)
    $('.bodyContainer').prepend(newElement);
    sideItems.push(direction);
}

function createElement(direction, currentClass)
{
    var element = [];
    element.push('<div class="directionLblCont" data-direction=' + direction + '>');
    switch(currentClass)
    {
        case "directionIcon":
            element.push('<div class="directionIcon arrow ' + direction + ' directionBtn" value=' + direction + '></div>');
        break;

        case "directionLable" :
            element.push('<span class="directionLable">' + direction + '</span>');
        break;

        case "directionFirstLetter" :
            element.push('<span class="directionFirstLetter">' + direction.charAt(0) + '</span>');
        break;
    }
    element.push('</div>');

    return $(element.join(''));
}

function formatElement(element)
{
    var leftPosition = positionArray[Math.floor(Math.random() * positionArray.length)];
    var color = colors[Math.floor(Math.random() * colors.length)];
    $(element).css({
        "left": leftPosition  + 10 + "px",
        "background-color" : color
    }).show();
}

function createAudioElement(audioSrc) {
    audioElement = document.createElement('audio');
    audioElement.setAttribute('src', "sounds/"+ audioSrc + ".mp3");
}

function playSound() {
    if (enableSound) {
        audioElement.currentTime = 0;
        audioElement.play();
    }
}

function increaseTimer() {
    timerSeconds -= 100;
    clearTimer();
}

function reStart(isHome) {
    totalScore = 0;
    timerSeconds = 1000;
    gamelevel = 1;
    currentlevel = 1;
    initialTopPosition = 60;
    increasingTopPosition = 10;
    elementCreateInterval = 3000;
    audioElement, creationTimer;
    sideItems = [];
    positionArray = [];
    exitInverseLevel();
    $("#powerBtnContainer").hide();
    $(".directionLblCont").remove();
    $(".level").html("Level 1");
    $(".scoreCard").html("0")
    $("#game-overlay").show();
    $(".life.uheart").removeClass("uheart");
    $("#gameOverWrapper").hide();
    if(isHome)
    {
        $("#game-overlay").hide();
        $("#playWrapper").show().removeClass("pause");
        $("#homebtn").hide();
        $(".introContainer").show();
        setPlayAnimation();
    }
    else
    {
        $("#myModal").show();
        window.getStart = setInterval(countDown, 1000);
    }
}

function home() {
    reStart(true);
}

function pause() {
    clearTimer();
    $("#game-overlay").show();
    $("#playWrapper").addClass("pause").show();
    $("#homebtn").show();
}

function exitFromApp(){
    navigator.app.exitApp();
}

function removeDirection(direction)
{
    // $(".directionLblCont").stop();
    var currentElement =  $(".directionLblCont[data-direction=" + direction + "]").last();
    var currentDirection = direction.toLowerCase();
    var rightPosition = $(".bodyContainer").width() - 20;
    var bottomPosition = $(".bodyContainer").height() - 20;
    $(currentElement).addClass("removeElement");
    
    switch(direction)
    {
        case "Left":
            $(currentElement).animate({"left" : "10px"}, 70, function(){
                $(this).remove();
            });
        break;

        case "Right":
            $(currentElement).animate({"left" : rightPosition + "px"}, 70, function(){
                $(this).remove();
            });
        break;

        case "Up":
            $(currentElement).animate({"top" : "10px"}, 70, function(){
                $(this).remove();
            });
        break;

        case "Down":
            $(currentElement).animate({"top" : bottomPosition + "px"}, 70, function(){
                $(this).remove();
            });
        break;
    }
}

function Test()
{
    
    $(currentElement).remove();
}