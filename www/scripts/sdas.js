var directionArray = ["Up", "Down", "Right", "Left"];
var timer;
var totalScore = 0;
var timerSeconds = 800;
var gamelevel = 1;
var life = 3;
var initialTopPosition = 60;
var increasingTopPosition = 10;

function setRandomDirection()
{
    timer = setInterval(function(){ 
        increasePosition();
    },timerSeconds);
}
function increasePosition()
{
    var currentTopPosition = parseInt($(".directionLblCont").css("top")) + 10 + "px";
    $(".directionLblCont").css("top", currentTopPosition);
}
function clearTimer()
{
    clearInterval(timer);
}
function registerEvents()
{
    $(".right, .left, .up, .down").off("click").on("click", function(){
        calculatePoints(this);
    });
}
function calculatePoints(element)
{
    $(".directionBtn").addClass("disabled");
    var curDirection = $(".directionLblCont").text();
    var btnValue = $(element).attr("value");
    if(curDirection == btnValue)
    {
        $(".scoreCard").html(++totalScore);
        if(totalScore % 10 === 0) 
        {
            increaseTimer();
        }
    }
    else
    {
        clearTimer();
    }
}

function createElement()
{
    var direction = directionArray[Math.floor(Math.random() * directionArray.length)];
    $('.bodyContainer').prepend('<div class="directionLblCont">' + direction + '</div>');
}
function increaseTimer()
{
    timerSeconds -= 200;
    clearTimer();
    setRandomDirection();
}