var directionArray = ["Up", "Down", "Right", "Left"];
var colors = ["#EC407A", "#8E24AA", "#C2185B", "#673AB7", "#3F51B5", "#2196F3", "#3D5AFE", "#009688", "#43A047", "#E65100", "#E64A19"];
var timer;
var totalScore = 0;
var sideItems = [];
var timerSeconds = 800;
var elementCreateInterval = 3000;
var gamelevel = 1;
var currentlevel = 1;
var life = 3;
var initialTopPosition = 60;
var increasingTopPosition = 10;
var audioElement;
var positionArray = [];
var creationTimer;
var enableSound = localStorage.getItem("sidEffectsEnableSound")? JSON.parse(localStorage.getItem("sidEffectsEnableSound")):true;
var enableVibration = true;
var highScore = 0;
var heartLevel;
if(!localStorage.getItem("sideEffectsNewUser"))
{
    localStorage.setItem("sideEffectsNewUser", true);
}

var powerBtn = {
    "Left" : 0,
    "Down" : 0,
    "Up"  : 0,
    "Right" : 0
};
var prevDirection;
var currentDirection;

function setHighScore() {
    var previousHighScore = localStorage.getItem("sidEffectsHighScore")
    if (previousHighScore) {
        if (previousHighScore < totalScore) {
            localStorage.setItem("sidEffectsHighScore", totalScore);
        }
    } else {
        localStorage.setItem("sidEffectsHighScore", totalScore);
    }
}

function newElementCreateInterval() {
    var rand = Math.round(Math.random() * (elementCreateInterval)) + 500;
    creationTimer = setTimeout(function () {
        createElement(creationTimer);
        increaseCreateElementInterval();
        newElementCreateInterval();
    }, rand);
}

function increaseCreateElementInterval() {
    if (currentlevel != gamelevel && elementCreateInterval>=0) {
        elementCreateInterval -= 400;
        clearInterval(creationTimer);
        currentlevel = gamelevel;
		console.log(elementCreateInterval);
    }
}

function setRandomDirection() {
    timer = setInterval(function () {
        increasePosition();
    }, timerSeconds);
}

function increasePosition() {
    var directionLableHeight = $(".directionLblCont").height();
    var bodyContainerHeight = $(".bodyContainer").height();
    var bodyContainerTop = $(".bodyContainer").offset().top;
    var isLimitReached = false;
    if ($(".directionLblCont").length) {
        if ($(".directionLblCont").last().offset().top >= bodyContainerHeight + bodyContainerTop - directionLableHeight - 10) {
            isLimitReached = true;
        }
        if ($(".directionLblCont").last().offset().top >= bodyContainerHeight - bodyContainerTop - directionLableHeight) {
            $("#dangerZone").show();
        } else {
            $("#dangerZone").hide();
        }
    }
    if (isLimitReached) {
        endGame();
    } else {
        $(".directionLblCont").each(function (index) {
            var currentTopPosition = parseInt($(this).css("top")) + 10 + "px";
            $(this).css("top", currentTopPosition);
        });
    }
}

function clearTimer(timerAttr) {
    if (timerAttr) {
        clearTimeout(creationTimer);
    } else {
        clearInterval(timer);
        clearTimeout(creationTimer);
    }
}

function registerEvents() {
    $(".right, .left, .up, .down").off("click").on("click", function () {
        calculatePoints(this);
    });
    $(".pause").off("click").on("click", function () {
        pause();
    });
    $("#continueBtn").off("click").on("click", function () {
        $("#continueBtn").hide();
        $("#game-overlay").hide();
        setRandomDirection();
        newElementCreateInterval();
    });

    $("#setMusic").off("click").on("click", function () {
        if ($(this).hasClass("volume-on")) {
            $(this).removeClass("volume-on").addClass("volume-off")
            enableSound = false;
        } else {
            $(this).removeClass("volume-off").addClass("volume-on")
            enableSound = true;
        }
        localStorage.setItem("sidEffectsEnableSound", enableSound);
    });

    $("#play").off(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend").on(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            $(this)
                .removeClass("bounce animated");
        }
    );

    $(".orgName").off(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend").on(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            $(this).removeClass("jello animated");
            clearInterval(window.setLogoAnimation);
            $(".ceyoneLogoContainer").hide();
            if(JSON.parse(localStorage.getItem("sideEffectsNewUser")))
            {
                localStorage.setItem("sideEffectsNewUser", false);
                $(".descContainer").show();
                $("#playWrapper").hide();
        }
            else
            {
                $(".introContainer").show();
                $("#playWrapper").show();
            }
        }
    );
    $("#levelWrapper").off(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend").on(
        "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend",
        function () {
            $(this)
                .removeClass("slideInUp animated").hide();
        }
    );

    $(".powerBtn").off("click").on("click", function(){
        if(!$(this).hasClass("disabled"))
        {
            $(this).addClass("disabled").removeClass("bounceIn animated");
            $($(".directionLblCont[data-direction=" + $(this).data("direction") + "]")).remove();
        }
    });
    $(".descNextBtn").off("click").on("click", function(){
        if($(this).text() != "Done")
        {
            $(".lifeDescCont").addClass("bounceOutLeft animated").hide();
            $(".powerBtnDescCont").show().addClass("bounceInRight animated");
            $(this).text("Done")
}
        else
        {
            $(".descSkip").trigger("click");
        }
    });
    $(".descSkip").off("click").on("click", function(){
        $(".descContainer").hide();
    });
}

function endGame() {
    clearTimer();
    setHighScore();
    $("#HighScore").html(localStorage.getItem("sidEffectsHighScore"));
    $(".highScoreLbl").html(localStorage.getItem("sidEffectsHighScore"));
    $("#score").html($(".scoreCard").html());
	$("#dangerZone").hide();
    $("#game-overlay").show();
    $("#gameOverWrapper").show();
}

function calculatePoints(element) {
    var btnValue = $(element).attr("value");
    if(!prevDirection)
    {
        prevDirection = btnValue;
    }
    if (sideItems.indexOf(btnValue) != -1) {
		currentDirection = btnValue;
        createAudioElement("blast");
        playSound();
        enablePowerBtn(btnValue);
        playSound();
        $(".scoreCard").html(++totalScore);
        levelUp(totalScore);
		$(".directionLblCont[data-direction=" + btnValue + "]").last().remove();
        var sideItemIndex = sideItems.indexOf(btnValue);
        sideItems.splice(sideItemIndex, 1);
        if (sideItems.length == 0)
            createElement();
        if (currentlevel != gamelevel  && timerSeconds >= 201) {
            increaseTimer();
            setRandomDirection();
            newElementCreateInterval();
        }

    }
    else{
        $(".life").not(".uheart").last().addClass("uheart");
        createAudioElement("error");
        playSound();
        heartLevel = gamelevel;
        if($(".life").not(".uheart").length == 0)
            endGame();
    }
}

function enablePowerBtn(btnValue) {
    if(gamelevel >= 5)
    {
        if(prevDirection == btnValue)
        {
            powerBtn[btnValue] +=1;
            if(powerBtn[btnValue] >=3)
            {
                $(".powerBtn" + btnValue).removeClass("disabled").addClass("bounceIn animated");
            }
        }
        else
        {
            powerBtn[prevDirection] = 0;
            powerBtn[btnValue] +=1;
            prevDirection = btnValue;
        }
    }
}

function levelUp(totalScore) {
    if (totalScore % 25 == 0) {
        gamelevel = parseInt(totalScore / 25) + 1;
        if(gamelevel == heartLevel + 2 && $(".life.uheart").length){
            $(".life.uheart").first().removeClass("uheart");
            heartLevel = gamelevel -1;
        }
        var curColor = colors[Math.floor(Math.random() * colors.length)];
        $(".level").html(gamelevel);
        $("#levelWrapper").css("color",curColor).show();
        $("#levelWrapper").addClass("slideInUp animated");
    }
    if(gamelevel == 5)
    {
        $("#powerBtnContainer").show().addClass("bounce animated");
    }
}

function createElement() {
    var direction = directionArray[Math.floor(Math.random() * directionArray.length)];
    var newElement = $('<div class="directionLblCont" data-direction=' + direction + '><div class="directionIcon arrow ' + direction + ' directionBtn" value=' + direction + '></div><span class="directionLable">' + direction + '</span><span class="directionFirstLetter">' + direction.charAt(0) + '</span></div>');
    var eleClasses = ["directionIcon", "directionLable", "directionFirstLetter"];
    var randomClass = eleClasses[Math.floor(Math.random() * (gamelevel <= 3 ? gamelevel : 3))];
    newElement.find("[class*='direction']").hide();
    newElement.find("." + randomClass).show();
    var curColor = colors[Math.floor(Math.random() * colors.length)];
    $(newElement).css("background-color", curColor);
    $(newElement).hide();
    $('.bodyContainer').prepend(newElement);
    positionElement(newElement);
    sideItems.push(direction);
}

function positionElement(ele) {
    $(ele).css("left", positionArray[Math.floor(Math.random() * positionArray.length)] + 10 + "px").show();
}

function createAudioElement(audioSrc) {
    audioElement = document.createElement('audio');
    audioElement.setAttribute('src', "sounds/"+ audioSrc + ".mp3");
}

function playSound() {
    if (enableSound) {
        audioElement.currentTime = 0;
        audioElement.play();
    }
}

function increaseTimer() {
    timerSeconds -= 200;
    clearTimer();
}

function reStart(isHome) {
    totalScore = 0;
    sideItems = [];
    timerSeconds = 800;
    gamelevel = 1;
    currentlevel = 1;
    life = 3;
    initialTopPosition = 60;
    increasingTopPosition = 10;
    elementCreateInterval = 3000;
    audioElement;
    positionArray = [];
    creationTimer;
    $("#powerBtnContainer").hide();
    $(".directionLblCont").remove();
    $(".level").html("1");
    $(".scoreCard").html("0")
    $("#game-overlay").show();
    $(".life.uheart").removeClass("uheart");
    $("#gameOverWrapper").hide();
    if(isHome)
    {
        $("#game-overlay").hide();
        $("#playWrapper").show().removeClass("pause");
        $("#homebtn").hide();
        $(".introContainer").show();
        setPlayAnimation();
    }
    else
    {
        $("#myModal").show();
    window.getStart = setInterval(countDown, 1000);
}
}

function home() {
    //location.reload();
    reStart(true);
}

function pause() {
    clearTimer();
    $("#game-overlay").show();
    $("#playWrapper").addClass("pause").show();
    $("#homebtn").show();
}

function exitFromApp(){
    navigator.app.exitApp();
}